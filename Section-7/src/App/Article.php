<?php

namespace App;

class Article
{
    /**
     * @var string
     */
    public $title;

    public function getSlug()
    {
        $slug = trim($this->title);

        $slug = preg_replace('/\s+/', '_', $slug);
        //\w matches any word character (equal to [a-zA-Z0-9_])
        $slug = preg_replace('/[^\w+]/', '', $slug);

        return $slug;
    }
}