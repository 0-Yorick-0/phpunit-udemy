<?php

use PHPUnit\Framework\TestCase;

class QueueTest extends TestCase
{
    /**
     * @var Queue
     */
    protected $queue;

    /**
     * Appelée avant CHAQUE test, donc l'objet est réinitialisé à chaque fois
     */
    protected function setUp(): void
    {
        $this->queue = new Queue();
    }

    /**
     * Effectuée une seule fois au début de la suite de test
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Effectuée une seule fois à la fin de la suite de test
     */
    public static function tearDownAfterClass(): void
    {
    }

    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, $this->queue->getCount());
    }

    public function testAnItemIsAddedToTheQueue()
    {
        $this->queue->push('item');

        $this->assertEquals(1, $this->queue->getCount());
    }

    public function testAnItemIsRemoveFromTheQueue()
    {
        $this->queue->push('item');

        $item = $this->queue->pop();

        $this->assertEquals(0, $this->queue->getCount());
        $this->assertEquals('item', $item);
    }

    public function testAnItemIsRemovedFromTheFrontOfTheQueue()
    {
        $this->queue->push('first');
        $this->queue->push('second');

        $this->assertEquals('first', $this->queue->pop());
    }

    public function testMaxNumberOfItemsCanBeAdded()
    {
        for ($i = 0; $i < Queue::MAX_ITEMS; $i++) {
            $this->queue->push($i);
        }
        
        $this->assertEquals(Queue::MAX_ITEMS, $this->queue->getCount());
    }

    public function testExceptionThrownWhenAddingAnItemToAFullQueue(){
        for ($i = 0; $i < Queue::MAX_ITEMS; $i++) {
            $this->queue->push($i);
        }

        $this->expectException(QueueException::class);
        $this->expectExceptionMessage("Queue is full");
        $this->queue->push('one more');
    }
}
