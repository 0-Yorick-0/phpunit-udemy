<?php

use PHPUnit\Framework\TestCase;


class OrderFirstVersionTest extends TestCase
{
    public function testOrderIsProcessed()
    {
        //mock d'une classe qui n'existe pas encore
        $gateway = $this->getMockBuilder('PaymentGateway')
            ->setMethods(['charge'])
            ->getMock();

        $gateway->expects($this->once())
            ->method('charge')
            ->with($this->equalTo(200))
            ->willReturn(true);

        $order = new OrderFirstVersion($gateway);

        $order->amount = 200;

        $this->assertTrue($order->process());

    }

    public function testOrderIsProcessedUsingMockery()
    {
        $gateway = Mockery::mock('PaymentGateway');

        $gateway->shouldReceive('charge')
            ->once()
            ->with(200)
            ->andReturn(true);

        $order = new OrderFirstVersion($gateway);

        $order->amount = 200;

        $this->assertTrue($order->process());
    }
}
