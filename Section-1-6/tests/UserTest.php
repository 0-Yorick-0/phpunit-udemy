<?php


use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testReturnsFullName()
    {
        $user = new User();

        $user->firstName = "Bob";
        $user->surname = "Leponge";

        $this->assertEquals('Bob Leponge', $user->getFullName());
    }

    public function testFullNameIsEmptyByDefault()
    {
        $user = new User();

        $this->assertEquals('', $user->getFullName());
    }

    public function testNotificationIsSent()
    {
        $user = new User;

        $mock = $this->createMock(Mailer::class);
        $mock->expects($this->once())
            ->method('sendMessage')
            ->with($this->equalTo('ash@nostromo.com'), $this->equalTo("Kill Everyone on board but the baby!"))
            ->willReturn(true);

        $user->setMailer($mock);
        $user->email = 'ash@nostromo.com';

        $this->assertTrue($user->notify("Kill Everyone on board but the baby!"));
    }

    public function testCannotNotifyUserWithNoEmail()
    {
        $user = new User;

        $mock = $this->getMockBuilder(Mailer::class)
            //aucune des méthodes originales ne sera stubbée, donc elles travailleront
            ->setMethods(null)
            ->getMock();

        $user->setMailer($mock);

        $this->expectException(Exception::class);

        $user->notify("Hello");
    }
}
