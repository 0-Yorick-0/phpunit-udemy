<?php


class MockTest extends \PHPUnit\Framework\TestCase
{
    public function testMock()
    {
        $mock = $this->createMock(Mailer::class);

        $mock->method('sendMessage')
            ->willReturn(true);

        $result = $mock->sendMessage('ash@nostromo.com', 'Yes Father');

        $this->assertTrue($result);
    }
}