<?php


class User
{
    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function setMailer(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim("$this->firstName $this->surname");
    }

    /**
     * @param string $message
     * @return bool
     */
    public function notify($message)
    {
        return $this->mailer->sendMessage($this->email, $message);
    }

}