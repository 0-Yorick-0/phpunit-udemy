<?php

/**
 * Roxed by :
 * User: yorickferlin
 * Date: 28/02/2020
 * No shit !
 */

use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    public function testSendMessageReturnsTrue()
    {
        $this->assertTrue(Mailer::send('ellenRipley@nostrom.com', 'argh!'));
    }
    
    public function testInvalidArgumentExceptionIfEmailIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        
        Mailer::send('', '');
    }
}
