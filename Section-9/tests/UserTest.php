<?php

/**
 * Roxed by :
 * User: yorickferlin
 * Date: 28/02/2020
 * No shit !
 */

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }
    
    public function testNotifyWithoutSetterReturnsTrue()
    {
        $user = new User('ellenRipley@nostromo.com');
        
        $user->setMailerCallable(function() {
            echo "mocked";
            
            return true;
        });
        
        $this->assertTrue($user->notifyWithoutSetter('Argh!'));
    }
    
    /**
     * @see http://docs.mockery.io/en/latest/reference/creating_test_doubles.html#creating-test-doubles-aliasing
     */
    public function testNotifyReturnsTrue()
    {
        $user = new User('ellenRipley@nostromo.com');
        
        $mailer = Mockery::mock('alias:Mailer');
        
        $mailer->shouldReceive('send')
            ->once()
            ->with($user->email, 'Argh')
            ->andReturn(true);
        
        $this->assertTrue($user->notify('Argh'));
    }

}
