<?php

use PHPUnit\Framework\TestCase;

class AbstractPersonTest extends TestCase
{
    public function testNameAndTitleIsReturned()
    {
        $doctor = new Doctor('Who');

        $this->assertEquals('Dr. Who', $doctor->getNameAndTitle());
    }

    /**
     * @see https://phpunit.readthedocs.io/en/7.4/test-doubles.html?highlight=getmockforabstractclass#mocking-traits-and-abstract-classes
     */
    public function testNameAndTitleIncludesValueFromGetTitle()
    {
        //astuce pour ne pas utiliser le bordélique constructeur de 'getMockForAbstractClass()'
        $mock = $this->getMockBuilder(AbstractPerson::class)
            ->setConstructorArgs(['Who'])
            ->getMockForAbstractClass();

        $mock->method('getTitle')
            ->willReturn('Dr.');

        $this->assertEquals('Dr. Who', $mock->getNameAndTitle());
    }

}
